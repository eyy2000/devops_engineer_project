import web
import mysql.connector
import uuid
import datetime
import time
import re
from multiprocessing import Process
import threading
import requests

import traceback

#server login parameters
host_str = "localhost"
user_str = "eric"
passwd_str = "password1"

#connect to server
mydb = mysql.connector.connect(
	host=host_str,
	user=user_str,
	passwd=passwd_str
)

#define db name
db_name = "mydatabase"

#initialize table
mycursor = mydb.cursor(buffered=True)

mycursor.execute("CREATE DATABASE IF NOT EXISTS " + db_name)
mycursor.execute("USE " + db_name)
mycursor.execute("""CREATE TABLE IF NOT EXISTS Resources (
						resource_id varchar(255) NOT NULL,
						hash_value varchar(255) NOT NULL UNIQUE,
						fortinet_detection_name varchar(255),
						scan_date DATE,
						number_of_engines int,
						query_date DATE,
						PRIMARY KEY (resource_id)

				)""")
mycursor.execute("""CREATE TABLE IF NOT EXISTS Users (
					user_id varchar(255) NOT NULL,
					number_of_requests int NOT NULL,
					fulfilled_requests int DEFAULT 0,
					created_date DATE,
					PRIMARY KEY (user_id)
				)""")
mycursor.execute("""CREATE TABLE IF NOT EXISTS Owns (
					user_id varchar(255),
					resource_id varchar(255),
					FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
					FOREIGN KEY (resource_id) REFERENCES Resources(resource_id) ON DELETE CASCADE
				)""")

mycursor.execute("SELECT DATE_FORMAT(scan_date, '%Y/%m/%d %H:%i:%s') FROM Resources")
mycursor.execute("SELECT DATE_FORMAT(query_date, '%Y/%m/%d %H:%i:%s') FROM Resources")
mycursor.execute("SELECT DATE_FORMAT(created_date, '%Y/%m/%d %H:%i:%s') FROM Users")

mydb.commit()
mydb.close()

#API key
api_key = 'f0434589f84efd43c3aa644ce06cb6eed704c13896c737b67d71d8b1ed3e2c34'

#class handling front end operations
class Index(threading.Thread):
	page_get_file = """
	<html>
		<head>
		</head>
		<body>
			<form method="POST" enctype="multipart/form-data" action="">
				Please enter data on only one of the following form
				<br>
				Upload file:
				<input type="file" name="myfile" />
				<br>
				View report via uuid:
				<input type="text" name="uuid_string_input" />
				<br>
				<input type="submit" />

			</form>
		</body>
	</html>
	"""

	page_successful_file_uplode = """
	<html>
		<head>
		</head>
		<body>
			your uuid is: {uuid_string}
			<form action="/">
				<input type="submit" value="return home"/>
			</form>
		</body>
	</html>
	"""

	page_unsuccessful_file_uplode = """
	<html>
		<head>
		</head>
		<body>
			Upload failed, please try again
			<br>
			Reason:
			<br>
			{error_message}
			<form action="/">
				<input type="submit" value="return home"/>
			</form>
		</body>
	</html>
	"""

	page_successful_report = """
	<html>
		<head>
			<style>
				td {{
					text-align:center
				}}
			</style>
		</head>
		<body>
			<h2> Report </h2>
			<br>
			reports ready: {reports_ready}/{total_reports}
			<br>
			<form action="/">
				<input type="submit" value="return home"/>
			</form>
			<br>
			<table style="width:100%">
				<tr>
					<th>hash_value (MD5 or Sha256)</th>
					<th>Fortinet detection name</th>
					<th>Scan Date</th>
					<th>Number of engines detected</th>
				{table_data}
			</table>
		</body>
	</html>
	"""
	def run(self):
		#define urls
		urls = ('/', 'Index')
		app = web.application(urls, globals())
		app.run()

	def GET(self):
		return self.page_get_file

	def POST(self):
		#parse input file
		f = web.input(myfile={})
		#parse input uuid
		uuid_string_input = web.input(uuid_string_input="")['uuid_string_input']

		#return table if uuid is given
		if uuid_string_input != "":
			#connect to db
			db = mysql.connector.connect(
				host=host_str,
				user=user_str,
				passwd=passwd_str
			)

			cursor = db.cursor()
			cursor.execute("USE " + db_name)

			report_query_string = "SELECT * FROM Resources JOIN Owns ON Resources.resource_id = Owns.resource_id WHERE user_id = %(user_id_string)s AND number_of_engines IS NOT NULL"
			
			try:
				#get report stored on MySql
				cursor.execute(report_query_string, {'user_id_string': uuid_string_input})
				select_result = cursor.fetchall()

				tmp_str = ""
			
				#construct report table
				for result in select_result:
					tmp_str += "<tr>"
					tmp_str += "<td>" + result[1] + "</td>"
					if result[2] is None:
						tmp_str += "<td>" + "</td>"
					else:
						tmp_str += "<td>" + result[2] + "</td>"
					if result[3] is None:
						tmp_str += "<td>" + "</td>"
					else:
						tmp_str += "<td>" + str(result[3]) + "</td>"
					tmp_str += "<td>" + str(result[4]) + "</td>"
					tmp_str += "</tr>"

				get_report_count_query_str = "SELECT number_of_requests FROM Users WHERE user_id = %(user_id_string)s"
				cursor.execute(get_report_count_query_str, {'user_id_string': uuid_string_input})
				report_number_result = cursor.fetchall()
				return self.page_successful_report.format(reports_ready=str(len(select_result)),total_reports=report_number_result[0][0],table_data=tmp_str)

			except Exception as ex:
				print("fail to select for report query")
				print(ex)

		#check Virustotal for report if file is given
		else:
			resources = f['myfile'].value.decode().split()

			#connect to db
			db = mysql.connector.connect(
				host=host_str,
				user=user_str,
				passwd=passwd_str
			)
			
			#insert user into table
			cursor = db.cursor()
			cursor.execute("USE " + db_name)
			user_uuid = str(uuid.uuid1())
			insert_user_sql_stm = "INSERT INTO Users (user_id, number_of_requests, created_date) VALUES(%s,%s,%s)"
			try:
				now = datetime.datetime.now()
				cursor.execute(insert_user_sql_stm, (user_uuid, str(len(resources)), now))
			except Exception as ex:
				print("failed to insert values to user")
				print(ex)
				db.rollback()
				return self.page_unsuccessful_file_uplode.format(error_message = ex)

			#insert resources into table
			insert_resources_sql_stm = "INSERT INTO Resources (resource_id, hash_value, fortinet_detection_name, scan_date, number_of_engines, query_date) VALUES (%s,%s,%s,%s,%s,%s)"
			insert_owns_sql_stm = "INSERT INTO Owns (user_id, resource_id) VALUES (%s,%s)"
			select_uuid_sql_stm = "SELECT resource_id FROM Resources WHERE hash_value = %(hash_value_string)s"
			select_error_message = "Found {resource_id_count} resource_id"
			for resource in resources:
				try:
					resource_uuid = str(uuid.uuid1())
					cursor.execute(insert_resources_sql_stm, (resource_uuid, resource, None, None, None, None))
				except Exception as ex:
					pattern = re.compile(".* Duplicate entry '[^']+' for key 'hash_value'")
					if not pattern.match(str(ex)):
						print("failed to insert to resources")
						print(ex)
						db.rollback()
						return self.page_unsuccessful_file_uplode.format(error_message = ex)
					else:
						#set the uuid to be the existing one
						try:
							cursor.execute(select_uuid_sql_stm, {'hash_value_string': resource})
							select_result = cursor.fetchall()
						
							if len(select_result) != 1:
								db.rollback()
								return self.page_unsuccessful_file_uplode.format(error_message = select_error_message.format(resource_id_count=str(len(select_result))))
							resource_uuid = select_result[0][0]
						except Exception as ex_select_existing_resource_id:
							print("failed to select existing resource id")
							print(ex_select_existing_resource_id)
							db.rollback()
							return self.page_unsuccessful_file_uplode.format(error_message = ex_select_existing_resource_id)
						
				#insert owns relationship into table
				try:
					cursor.execute(insert_owns_sql_stm, (user_uuid, resource_uuid))
				except Exception as ex_owns:
					print("failed to insert to owns")
					print(ex_owns)
					db.rollback()
					return self.page_unsuccessful_file_uplode.format(error_message = ex_owns)
			

			#close connection and commit
			cursor.close()
			db.commit()
			db.close()
			return self.page_successful_file_uplode.format(uuid_string = user_uuid)

class Background(threading.Thread):
	

	def run(self):

		sql_select_statement = "SELECT hash_value FROM Resources WHERE (number_of_engines IS NULL) OR (scan_date < subdate(current_date, 1) AND query_date < subdate(current_date, 1)) LIMIT 4"
		sql_update_statement = "UPDATE Resources SET fortinet_detection_name = %(fortinet_detection_name_string)s, scan_date = %(scan_date_string)s, number_of_engines = %(number_of_engines_string)s, query_date = %(query_date_string)s WHERE hash_value = %(hash_value_string)s"
		select_all_user_statement = "SELECT user_id FROM Users"
		select_user_complete_request_statement = """SELECT user_id, hash_value FROM (Owns JOIN Resources ON Owns.resource_id = Resources.resource_id) WHERE (number_of_engines IS NOT NULL) AND user_id = %(user_id_string)s"""
		update_user_complete_request_statement = "UPDATE Users SET fulfilled_requests = %(fulfilled_requests_string)s WHERE user_id = %(user_id_string)s"
		janitor_sql_string = "DELETE FROM Users WHERE number_of_requests = fulfilled_requests AND created_date < subdate(current_date, 1)"
		url = 'https://www.virustotal.com/vtapi/v2/file/report'
		fortinet_name = "Fortinet"
		while (True):
			#connect to db
			db = mysql.connector.connect(
				host=host_str,
				user=user_str,
				passwd=passwd_str
			)

			cursor = db.cursor()
			cursor.execute("USE " + db_name)

			#select resource for update
			print("updating")
			try:
				cursor.execute(sql_select_statement)
			except Exception as ex:
				print("failed to select in background")
				print(ex)

			#query virus total for info
			select_result = cursor.fetchall()
			for result in select_result:
				print("result")
				print(result)
				params = {'apikey': api_key, 'resource': result[0]}
				response = requests.get(url, params=params)
				if 'json' in response.headers.get('Content-Type'):
					json_response = response.json()
					if json_response['response_code'] == 1:
						print(json_response['scans'][fortinet_name]['result'])
						print(json_response['positives'])
						print(json_response['scan_date'])
						print(result[0])
						print('good_response')
						#update Resrouce table
						try:
							print("***UPDATE***")
							now = datetime.datetime.now()
							cursor.execute(sql_update_statement, {'fortinet_detection_name_string':json_response['scans'][fortinet_name]['result'], 'scan_date_string':json_response['scan_date'], 'number_of_engines_string':json_response['positives'], 'hash_value_string':result[0], 'query_date_string': now})

						except Exception as ex_update:
							print("failed to update")
							print(ex_update)
					else:
						try:
							print("***UPDATE***")
							now = datetime.datetime.now()
							cursor.execute(sql_update_statement, {'fortinet_detection_name_string':None, 'scan_date_string':None, 'number_of_engines_string':-1, 'hash_value_string':result[0], 'query_date_string':now})
							
						except Exception as ex_update:
							print("failed to update")
							print(ex_update)

				else:
					print("non json response")
					print(response.content)
				db.commit()

			#update fulfilled requests count
			try:
				cursor.execute(select_all_user_statement)
				sel_result = cursor.fetchall()
				
				for result in sel_result:
					
					cursor.execute(select_user_complete_request_statement, {'user_id_string': result[0]})
					select_user_complete_request_statement_result = cursor.fetchall()

					cursor.execute(update_user_complete_request_statement, {'fulfilled_requests_string': len(select_user_complete_request_statement_result),'user_id_string': result[0]})
					db.commit()
			except Exception as ex:
				print("failed to update user fulfilled requests")
				print(ex)

			#remove users who has had requests fulfilled for more than a day
			try:
				cursor.execute(janitor_sql_string)
				db.commit()
			except Exception as ex:
				print("failed to delete old accounts")
				print(ex)

			#close connection, wait for the next update loop
			db.close()
			print("end update")
			time.sleep(15)
			





#run main loop
if __name__ == "__main__":
	i = Index()
	i.start()
	b = Background()
	b.start()